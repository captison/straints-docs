---
currentMenu: glossary
---

# Glossary of Terms

###### **constraint** <a name="constraint"></a>  
A unit of validation logic that defines criteria that an object property is validated against.

###### **constraint object** <a name="constraint"></a>  
An object definition in the VSD having at least a `test` or `poll` attribute.

###### **context** <a name="context"></a>  
A unit of validation logic that defines criteria that an object is validated against.

###### **rule** <a name="rule"></a>  
A reference to a __validation unit__ in the VSD optionally prefixed by a __target property__.  The validation unit can be a validator method, a context, a constraint, or an array of constraints.

###### **rule expression** <a name="rule-expression"></a>  
Two or more __rules__ compared via logic gates. Or even one __rule__ preceded by a `not` gate.

###### **target property** <a name="target-property"></a>  
The property to be validated for a given __rule__.

###### **test method** <a name="test-method"></a>  
Synonymous with 'validator method'.  This refers to a method on the validator implementation.

###### **validation session** <a name="session"></a>  
The validation activity that takes place within a single call to `.validate()`.

###### **validation unit** <a name="validation-unit"></a>  
A unit (or aggregation of units) of validation logic.  A __test method__, __context__, __constraint__, or constraint array.
