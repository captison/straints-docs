---
currentMenu: running
---

# Running Validations

To run a validation call `.validate()` on a _straints_ instance.

For example, given the following arguments,

```js
var target = { username: 'alexa123', password: 'partytime56' };
var contexts = [ 'login' ];
var validate = (result, info) => { ... };
var success = results => { ... };
var failure = results => { ... };
```

validation can be run like so

```js
instance.validate(target, contexts, validate).then(success, failure); // promise
```

Here is a quick explanation of the arguments.

* **target** (object)  
The object to be validated.

* **contexts** (string|array)  
One or more contexts to be validated against.  This can be a comma-delimited string or an array.

* **validate(result: boolean, info: object)**  
Callback executed for every constraint test during validation.

* **success(results: object)**  
Callback to be executed when validation completes successfully.

* **failure(results: object)**  
Callback to be executed if an error occurred during validation.

## Overriding A Validation

Individual validations can be managed by providing a function as the last parameter to `.validate()`.

This callback will receive the following parameters

* **result** (boolean)  
The boolean result of the validation test.  

* **info** (object)  
Additional information pertaining to the validation test.  

    * **target** (object)  
    Current validation target object (same as `starget` unless nested)  

    * **starget** (object)  
    Session validation target object (always the original validation target)

    * **name** (string)  
    Current target object property name

    * **sname** (string)  
    Session target object property name

    * **rule** (object)  
    Validating constraint object

    * **level** (string)  
    Name of the current validation level.

If this callback is provided then it MUST return a boolean value.  Simply return `result` if nothing else.

```js
function(result, info) { return result; }
```


## Validation Results

The validation results are provided to both the `success` and `failure` callbacks.  In case of failure, obviously, the results object may not be complete.

You will find the following items in this object.

+ **target** (object)  
The object originally passed to `.validate()`.

+ **contexts** (array)  
The contexts involved in the validation session.  This includes the contexts passed to `.validate()` as well as any included by those contexts, recursively.

+ **tested.xxx** (objects)  
Map of property to constraint ids of constraints whose tests were specified at validation level 'xxx'.

+ **constraints** (object)  
All of the constraints directly involved in the validation session keyed by their ids.

+ **isComplete** (boolean)  
A boolean value that will be true if validation completed successfully.

+ **error** (any)  
If the `failure` callback is executed, this will have error information.

+ **findConstraints(pname: string, level: string, value: boolean|null): array**  
  **findProperties(cname: string, level: string, value: boolean|null): array**  
  Functions that return an array of constraint or property names by property or constraint, validation level, and value.

    + **pname** (string)  
    Object property name to get constraints for. If not given then all properties are checked.

    + **cname** (string)  
    Constraint identifier to get properties for. If not given then all constraints are checked.

    + **level** (string)  
    The validation level to check. If not given then `constrain` is assumed.

    + **value** (boolean|null)  
    Value to check for. This can only be `true`, `false`, or `null`. If not given then `false` is assumed.  

+ **validFor(level: string): boolean**  
For the given validation level, returns `true` if all tests passed, `false` if any failed, and `null` if none were run or if the level does not exist.

+ **valid(): boolean**  
Returns true if validation completed successfully and no `constrain`-level tests failed.

+ **payload(cname: string): any**  
Returns the payload data for the named constraint.


### For Example...

For a given target object and _straints_ instance:

```js
var target = { name: 'Alexa', birthdate: null, attractive: true };
var contexts = [ 'contextOne', 'contextThree' ];
var instance = straints({ levels: [ 'suggest' ] });
```

Here's a simple example that illustrates what you might do when validation fails.

```js
instance.validate(target, contexts).then
(
    results =>
    {
        if (!results.valid())
        {
            // names of constraints that failed for property 'name'...
            var failNames = results.findConstraints('name');
            // then get the constraint objects that failed
            var constraints = failNames.map(name => results.constraints[name]);
            // or get names of all failing properties
            var failProps = results.findProperties();
        }    
    }
);
```

Similarly, if you want to check those validation suggestions...

```js
instance.validate(target, contexts).then
(
    results =>
    {
        if (!results.validFor('suggest'))
        {
            // names of constraints that failed for property 'name'...
            var failNames = results.findConstraints('name', 'suggest');
            // then get the constraint objects that failed
            var constraints = failNames.map(name => results.constraints[name]);
            // or get names of all failing properties
            var failProps = results.findProperties(null, 'suggest');
        }
    }
);
```

> __NOTE:__  
The results object collects information about the validation session as it runs, thus its state is undetermined if validation fails to complete.  However, it is passed to the `failure` callback nonetheless.  In this scenario, only its `target`, `isComplete`, and `error` properties can be relied upon.
