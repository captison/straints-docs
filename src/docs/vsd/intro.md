---
currentMenu: vsd
---

# The Validation Schema Definition

A Validation Schema Definition (**VSD**) is JSON that declaratively defines the validations that will verify objects in your application.  Individual validation tests ultimately take place at the property level, but these combined property verifications ensure that an object's structure and data conform to a given set of requirements.

Using the VSD you can

- validate properties against static or dynamic values or each other
- validate objects via sets of property validations
- validate nested objects or arrays of objects
- validate against aggregate validation results
- perform validations on properties or objects conditionally
- reuse validation definitions at the object and property level

Learn about the different components of the VSD using the submenu items to the left.

> __NOTE__  
The VSD examples throughout this documentation use YAML as it is a bit easier to read than the equivalent JSON.  
