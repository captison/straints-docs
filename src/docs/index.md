---
currentMenu: starting
---

# Installation

_Straints_ can be installed through [npm](https://nodejs.org).

To add _straints_ to your project via npm do

```bash
$ npm install straints --save
```

To use in the browser link in one of the js files.

```html
<script src=".../straints.min.js"></script>
```

Alternatively, you can use the [unpkg](https://unpkg.com) CDN.

```html
<script src="https://unpkg.com/straints/dist/straints.min.js"></script>
```

Once you've got the script in your page, you can do

```html
<script>
    var instance = straints({ /* config options */ });
</script>
```


# Configuration Options

Here's a list of the available configuration options.

- **`validator`** (object)  
Specify the test method implementation(s) to use here.  (As of v0.7.0 _straints_ no longer comes with a default validator method implementation)

- **`schema`** (object)  
Validation schema definition for the _straints_ instance.

- **`contexts`** (object)  
Default validation context(s) to use if none passed to the _straints_ instance.

- **`levels`** (string|array)  
Additional validation levels can be specified here with an array or comma-delimited string.


# Validators

As far as _straints_ is concerned, a validator is simply an object with validation methods (or test methods).  

```js
var instance = straints({ validator: myTestMethods });
// OR, if using multiple validator libraries
var instance = straints({ validator: { alyze: alyze.create(), myTestMethods } });
```

In the validation schema a reference to a test method is its full dot-notated path.


## Validation Methods

_Straints_ expects test methods to be defined in the following way:

```
function testMethod(value: any, ...args: any[]): boolean|Function|Promise
```

The test method must accept the test value as the first argument and may also accept or require additional arguments.  

_Straints_ handles the test method return value as follows:

- a **Promise** its `.then()` is called with success and error callbacks
- a **function** it is called with success and error callbacks
- a **boolean** then it is handled as a successful validation

If the value returned is none of the above or if the error callback is used then the validation session will be terminated.
