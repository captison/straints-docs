<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Straints | Documentation</title>

    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/font-awesome.min.css">
    <link rel="stylesheet" href="/css/highlight.dark.css">
    <link rel="stylesheet" href="/css/common.css">
    <link rel="stylesheet" href="/css/main.css">

    <script src="//code.jquery.com/jquery-1.12.4.min.js"></script>
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
    <script src="//yastatic.net/highlightjs/8.2/highlight.min.js"></script>

    <script>
      $(function() { hljs.initHighlightingOnLoad(); });
    </script>
  </head>
  <body>
    <div class="inner">

      <main class="container-fluid">
        <div class="row">

          <nav id="sidebar" class="col-sm-3 col-lg-2" role="navigation">
            <div class="logo">
              <a href="/index.html">
                <img src="/images/straints-logo.png" />
              </a>
            </div>
            <ul class="nav nav-pills nav-stacked">
                              <li class="starting ">
                  <a href="/docs/index.html">Getting Started</a>
                                  </li>
                              <li class="vsd ">
                  <a href="/docs/vsd/intro.html">The VSD</a>
                                      <ul>
                                              <li class="vsd-contexts active">
                          <a href="/docs/vsd/contexts.html">Contexts</a>
                        </li>
                                              <li class="vsd-constraints ">
                          <a href="/docs/vsd/constraints.html">Constraints</a>
                        </li>
                                              <li class="vsd-rules ">
                          <a href="/docs/vsd/rules.html">Rules</a>
                        </li>
                                          </ul>
                                  </li>
                              <li class="running ">
                  <a href="/docs/running.html">Running Validations</a>
                                  </li>
                              <li class="glossary ">
                  <a href="/glossary.html">Glossary</a>
                                  </li>
                              <li class="playground ">
                  <a href="/playground.html">Straints Playground</a>
                                  </li>
                          </ul>
          </nav>

          <section id="content" class="col-sm-offset-3 col-lg-offset-2 col-sm-9 col-lg-10">
            <h1 id="validation-contexts">Validation Contexts</h1>
<p>A context is a blueprint for how an object will be validated.  A context is identified by its path in the VSD and is recognized by <em>straints</em> as such if it has a child of one of the following:</p>
<ol>
<li>a <code>constrain</code>, <code>include</code>, or <code>nested</code> directive</li>
<li>any validation level specified in configuration</li>
</ol>
<p>For example,</p>
<pre><code class="language-yaml">person:
  constrain:
    name: [ is.notNull ]
    email: [ is.notNull, email ]
basketball:
  player:
    include: [ person ]
    constrain:
      position: [ is.playerPosition ]
  team:
    nested:
      coach:
        include: [ person ]
      players:
        nested:
          ____:
            include: [ basketball.player ]
    constrain:
      name: [ is.notNull ]
      coach: [ is.notNull ]
      players: [ is.notNull ]
is:
  - { name: notNull, test: 'null', flip: true }
  - { name: playerPosition, test: itemIn, params: [ [ point, guard, forward, water ] ] }</code></pre>
<p>in the above our named contexts are</p>
<ul>
<li>person</li>
<li>basketball.player</li>
<li>basketball.team</li>
<li>basketball.team.nested.coach</li>
<li>basketball.team.nested.players</li>
<li>basketball.team.nested.players.nested.____</li>
</ul>
<p>This is because the 'person' context has a <code>constrain</code> directive, 'basketball.player' has <code>include</code> and <code>constrain</code> directives, 'basketball.team.nested.players' has <code>nested</code>, and so on.</p>
<p>A context may define its validations through 3 primary directives:</p>
<ul>
<li><strong>constrain</strong> - defines the required property validations for an object</li>
<li><strong>include</strong> - conditionally includes additional contexts or portions thereof</li>
<li><strong>nested</strong> - defines sub-contexts to test the validity of child objects</li>
</ul>
<h2 id="the-constrain-directive">The Constrain Directive</h2>
<p>The <code>constrain</code> directive applies validation rules to properties by name for its parent context.  Any object being validated against that context will be required to satisfy these rules.</p>
<p>The usage of this directive is as follows:</p>
<pre><code class="language-yaml">(context name):
  constrain:
    (property 1):
      - (constraint A)
      - (constraint B)
      ...
    (property 2):
      - (constraint C)
      ...
    ...</code></pre>
<p>For example:</p>
<pre><code class="language-yaml">add_user:
  constrain:
    name:
      - { test: missing, flip: true }
    email:
      - { test: missing, flip: true }
      - { test: email }</code></pre>
<p>Here we have defined some rules for objects validated against the 'add_user' context.</p>
<ul>
<li>property 'name' must not be missing</li>
<li>property 'email' must not be missing</li>
<li>property 'email' must be a valid email</li>
</ul>
<p>It is also possible to specify validations by constraint.</p>
<pre><code class="language-yaml">(context name):
  constrain:
    ~(constraint A):
      - (property 1)
      - (property 2)
      ...
    ~(constraint B):
      - (property 2)
      ...
    ...</code></pre>
<p>Note the tildes (<code>~</code>) used here.  This lets the <em>straints</em> parser know that you are specifying validation rules by constraint rather than by property.</p>
<p>For example:</p>
<pre><code class="language-yaml">add_user:
  constrain:
    ~exists:
      - name
      - email
    ~email:
      - email</code></pre>
<p>This validates similarly to the previous example.</p>
<ul>
<li>property 'name' must exist</li>
<li>property 'email' must exist</li>
<li>property 'email' must be a valid email</li>
</ul>
<h3 id="the-quadruple-underscore">The Quadruple Underscore</h3>
<p>You may use <code>____</code> (quadruple underscore) as a property name in <code>constrain</code> to apply constraints to all properties that exist on the current validation target.</p>
<pre><code class="language-yaml">all_good:
  constrain:
    ____:
      - exists</code></pre>
<p>This syntax works best with arrays, but can also be used with objects.</p>
<h2 id="the-nested-directive">The Nested Directive</h2>
<p>Use the <code>nested</code> directive when an object contains child objects or arrays that also need to be validated.</p>
<p>Here's the syntax:</p>
<pre><code class="language-yaml">(context name):
  nested:
    (property 1):
      ...
    (property 2):
      ...
    ...</code></pre>
<p>Property names specified under <code>nested</code> are contexts by default and, therefore, enjoy all the benefits of being a context.</p>
<p>For example:</p>
<pre><code class="language-yaml">contact:
  constrain:
    address: [ exists ]
  nested:
    address:
      constrain:
        street: [ exists, string ]
        city: [ exists, string ]
        state: [ exists, string ]
        zipCode: [ exists, number ]</code></pre>
<p>In the above, the 'contact' context allows validating the 'address' property as an object.  It is also required to exist as per the <code>constrain</code> directive.  Note here that any 'address' value that is not an object will simply be skipped by the <code>nested</code> directive.  If we wish to make sure that 'address' is an object, we must further constrain it.</p>
<pre><code class="language-yaml">contact:
  constrain:
    address: [ exists, object ]</code></pre>
<p>Here's an example of how this could work with an array (assuming values are positional).</p>
<pre><code class="language-yaml">contact:
  constrain:
    address: [ exists, array ]
  nested:
    address:
      constrain:
        0: [ exists, string ]
        1: [ exists, string ]
        2: [ exists, string ]
        3: [ exists, number ]</code></pre>
<p>The <code>nested</code> directive (and <em>straints</em> in general) essentially treats arrays as objects with numeric keys.</p>
<h3 id="the-quadruple-underscore-1">The Quadruple Underscore</h3>
<p>While <code>nested</code> alone works well for single objects, what if you want to validate a list of objects?  </p>
<p>Remember our basketball VSD above?  Here's the nested 'players' part of the 'basketball.team' context.</p>
<pre><code class="language-yaml">basketball:
  team:
    nested:
      players:
        nested:
          ____:
            include: basketball.player</code></pre>
<p>We nest again inside of 'players' to get at the objects in the list.  Then you can use the <code>____</code> to apply the context to every element of the array.</p>
<p>So, for every player on the team,</p>
<ul>
<li>'name' cannot be null</li>
<li>'email' cannot be null</li>
<li>'email' must be a valid email address</li>
<li>'position' must be one of point, guard, forward, or water</li>
</ul>
<p>The quad-underscore will also work on an object of objects as well.  Just remember that <code>____</code> will apply its contextual validation rules to EVERY object found as a property value of the parent object.</p>
<h2 id="the-include-directive">The Include Directive</h2>
<p>Perhaps we wish to reuse object validations and combine them to create more comprehensive validation. Here's a short (contrived) example.  Imagine an e-commerce site that needs to capture details about the user making an order.</p>
<pre><code class="language-yaml">guest:
  constrain:
    ~exists: [ name, address, phone ]
    ~string: [ name, address, email ]
    phone: [ number ]
    email: [ email ]
login:
  constrain:
    ~exists: [ email, password ]
create_account:
  include: [ guest, login ]
  constrain:
    password: [ string, alphanumeric ]
    passwordConfirm:
      - exists
      - { test: equal, params: t.password }
    emailConfirm:
      - exists
      - { test: equal, params: t.email }</code></pre>
<p>During checkout, if the user did not wish to create an account the incoming data object might be validated as 'guest'.  Alternatively, validating with 'create_account' would be used if an account was desired.</p>
<p>The 'create_account' context includes the 'guest' and 'login' contexts.  This means that <strong>all</strong> of the configuration for those two contexts is also applied when validating against 'create_account'.</p>
<p>Let's break down this example by context and property to see what's getting validated.</p>
<ul>
<li>'guest'
<ul>
<li>'name' and 'address' must exist and be a string values.</li>
<li>'phone' must exist and be a number.</li>
<li>'email' must be string and a valid email address if it exists.</li>
</ul></li>
<li>'login'
<ul>
<li>'email' and 'password' must exist.</li>
</ul></li>
<li>'create_account'
<ul>
<li>'name' and 'address' must exist and be a string values.</li>
<li>'phone' must exist and be a number.</li>
<li>'email' must exist, be a string and a valid email address.</li>
<li>'password' must exist, be a string and be alphanumeric in value.</li>
<li>'passwordConfirm' must exist and be equal to 'password'.</li>
<li>'emailConfirm' must exist and be equal to 'email'.</li>
</ul></li>
</ul>
<p>As you can see, validations are merged under <code>constrain</code> so that a given property is validated against an aggregation of constraints across all included contexts. Duplicate constraints are discarded, however, so a given constraint in a set of merged contexts will run only once against a given property.</p>
<h3 id="partial-inclusion">Partial Inclusion</h3>
<p>We can also include only specific directives from a context if we wish.  For instance, we could rewrite the include for 'create_account' to include only the <code>constrain</code> directives from the contexts.</p>
<pre><code class="language-yaml">create_account:
  include: [ guest#constrain, login#constrain ]</code></pre>
<p>Simply prefix the directive (or validation level) name with a hashmark (<code>#</code>).</p>
<h3 id="conditional-inclusion">Conditional Inclusion</h3>
<p>Use a <code>condition</code> object to define which contexts will be included.</p>
<p>Imagine a situation where you need to select players for a basketball team.</p>
<pre><code class="language-yaml">potentialPlayer:
  include:
    - { if: greatShooter and greatPasser, then: starter, else: benchwarmer }</code></pre>
<p>Validating against 'potentialPlayer' would first pre-validate the object as 'greatShooter' and 'greatPasser'.  If these validate successfully, then the 'starter' context would be included with 'potentialPlayer', otherwise, the 'benchwarmer' context would be included.</p>
<p>Here are the parameters you can use to configure a <code>condition</code> object.</p>
<ul>
<li>
<p><strong><code>name</code></strong> (string)<br />
If you wish to reference the condition elsewhere in the VSD this makes it much easier.</p>
</li>
<li>
<p><strong><code>if</code></strong> (string)<br />
A rule or rule expression that, if <code>true</code>, includes the <code>then</code> contexts.  Otherwise, the <code>else</code> contexts will be included.</p>
</li>
<li>
<p><strong><code>then</code></strong> (array|string)<br />
The context(s) that will be included when the <code>if</code> condition is <code>true</code>.  If there is no <code>if</code> then inclusion is automatic.</p>
</li>
<li><strong><code>else</code></strong> (array|string)<br />
The context(s) that will be included when the <code>if</code> condition is <code>false</code>.  If there is no <code>if</code> then this is ignored.</li>
</ul>
<p>Remember that while the <code>if</code> parameter accepts a rule, <code>then</code> and <code>else</code> can only accept an array or comma-delimited string of context names.  Learn more about rules and rule expressions <a href="/docs/vsd/rules.html">here</a>.</p>
<blockquote>
<p><strong>NOTE</strong><br />
Validations that occur under an <code>if</code> condition are separate from the current validation session. They are not included in the results object nor do their executions generate callbacks.</p>
</blockquote>
<h2 id="additional-validation-levels">Additional Validation Levels</h2>
<p>In addition to the three directives, contexts can also be defined to have additional validation 'levels'.  These levels must first be defined in the configuration to allow <em>straints</em> to recognize them.  </p>
<pre><code class="language-js">var instance = straints({ levels: [ ... ] });</code></pre>
<p>Then you can use those level names as children of the context.  Their operation is identical to that of the <code>constrain</code> directive and their results are stored separately in the results object.</p>
<p>Note that you cannot use <code>include</code>, <code>nested</code>, or <code>constrain</code> as validation level names as they are reserved as context directives.</p>
          </section>

        </div>
      </main>

      <footer>
        <div class="container-fluid">
          <p class="text-muted">
            <span>
              <a href="https://bitbucket.org/captison/straints.git" title="repository">repo</a>
            </span>
            <span>
              <a href="https://bitbucket.org/captison/straints/src/master/CHANGELOG.md" title="changelog">changelog</a>
            </span>
            <span>
              <a href="https://bitbucket.org/captison/straints/issues" title="feedback">feedback</a>
            </span>
            <span>
              <a href="https://www.npmjs.com/package/alyze" title="validator">alyze</a>
            </span>
            <span>
              <a href="http://couscous.io" title="markdown website generator">couscous</a>
            </span>
            <span>
              &copy; 2017 <a href="http://captison.com" title="straints author">Captison</a>
            </span>
          </p>
        </div>
      </footer>
    </div>
        <!-- Livereload -->
        <script>
            document.write('<script src="http://' + (location.host || 'localhost').split(':')[0] +
                    ':35729/livereload.js?snipver=1"></' + 'script>')
        </script>
        <!-- End Livereload -->
    </body>
</html>
