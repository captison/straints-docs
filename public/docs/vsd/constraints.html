<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Straints | Documentation</title>

    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/font-awesome.min.css">
    <link rel="stylesheet" href="/css/highlight.dark.css">
    <link rel="stylesheet" href="/css/common.css">
    <link rel="stylesheet" href="/css/main.css">

    <script src="//code.jquery.com/jquery-1.12.4.min.js"></script>
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
    <script src="//yastatic.net/highlightjs/8.2/highlight.min.js"></script>

    <script>
      $(function() { hljs.initHighlightingOnLoad(); });
    </script>
  </head>
  <body>
    <div class="inner">

      <main class="container-fluid">
        <div class="row">

          <nav id="sidebar" class="col-sm-3 col-lg-2" role="navigation">
            <div class="logo">
              <a href="/index.html">
                <img src="/images/straints-logo.png" />
              </a>
            </div>
            <ul class="nav nav-pills nav-stacked">
                              <li class="starting ">
                  <a href="/docs/index.html">Getting Started</a>
                                  </li>
                              <li class="vsd ">
                  <a href="/docs/vsd/intro.html">The VSD</a>
                                      <ul>
                                              <li class="vsd-contexts ">
                          <a href="/docs/vsd/contexts.html">Contexts</a>
                        </li>
                                              <li class="vsd-constraints active">
                          <a href="/docs/vsd/constraints.html">Constraints</a>
                        </li>
                                              <li class="vsd-rules ">
                          <a href="/docs/vsd/rules.html">Rules</a>
                        </li>
                                          </ul>
                                  </li>
                              <li class="running ">
                  <a href="/docs/running.html">Running Validations</a>
                                  </li>
                              <li class="glossary ">
                  <a href="/glossary.html">Glossary</a>
                                  </li>
                              <li class="playground ">
                  <a href="/playground.html">Straints Playground</a>
                                  </li>
                          </ul>
          </nav>

          <section id="content" class="col-sm-offset-3 col-lg-offset-2 col-sm-9 col-lg-10">
            <h1 id="validation-constraints">Validation Constraints</h1>
<p>Constraints can be defined in arrays anywhere in the VSD.  They are put to use, however, within the <code>constrain</code> directive or other validation levels within a context or within rules or rule expressions.</p>
<h2 id="constraint-objects">Constraint Objects</h2>
<p>In the VSD, a constraint object may be defined with some combination of the following properties:</p>
<ul>
<li>
<p><strong><code>name</code></strong> (string)<br />
A name for the constraint object that makes it easier to reference from elsewhere in the VSD.</p>
</li>
<li>
<p><strong><code>test</code></strong> (string)<br />
A rule or rule expression that defines a validation test for a given property of the current validation target.</p>
</li>
<li>
<p><strong><code>poll</code></strong> (string)<br />
A rule or rule expression that defines a validation test for all properties of the current validation target.  This is ignored if <code>test</code> is given.</p>
</li>
<li>
<p><strong><code>results</code></strong> (string)<br />
A rule or rule expression used with <code>poll</code> to evaluate its aggregated validation test results.</p>
</li>
<li>
<p><strong><code>if</code></strong> (string)<br />
A rule or rule expression that, when <code>true</code>, allows <code>test</code> or <code>poll</code> to be executed.</p>
</li>
<li>
<p><strong><code>param</code></strong> (array)<br />
Additional parameter that is passed to test methods appearing in any rule or rule expression in the constraint.</p>
</li>
<li>
<p><strong><code>params</code></strong> (array)<br />
Additional parameters that are applied to test methods appearing in any rule or rule expression in the constraint.  This is ignored if <code>param</code> is given.</p>
</li>
<li>
<p><strong><code>property</code></strong> (string)<br />
Locks the target property used with any <code>test</code>, <code>poll</code>, or <code>if</code> rule expression.</p>
</li>
<li>
<p><strong><code>flip</code></strong> (boolean)<br />
Set to <code>true</code> to reverse the boolean value ultimately returned by <code>test</code> or <code>results</code>.</p>
</li>
<li><strong><code>payload</code></strong> (any)<br />
Arbitrary data to attach to the constraint that will be available in results object via <code>.payload()</code>.</li>
</ul>
<p>Remember that either <code>test</code> or <code>poll</code> is required for a constraint object.</p>
<h3 id="referencing-a-constraint">Referencing a Constraint</h3>
<p>Constraints are identified by the path to their definition in the VSD.</p>
<pre><code class="language-yaml">pizza:
  constrain:
    cheese:
      - { test: itemIn, param: [ mozzarella, provolone, jack ] }
    topping:
      - in.available.toppings
in:
  available:
    - { name: toppings, test: itemIn, param: [ pepperoni, mushroom, olives ] }</code></pre>
<p>This one-topping pizza validation schema has two referenceable constraints.</p>
<ul>
<li>pizza.constrain.cheese.0</li>
<li>in.available.toppings</li>
</ul>
<p>When a constraint has no 'name' specified then it's name is the index of its position in the array where it is defined.</p>
<p>Constraint arrays can also be referenced:</p>
<pre><code class="language-yaml">pizza:
  constrain:
    sauce: [ good.name ]
good:
  name:
    - [ exists, lowercase ]
    - { test: not longer, param: 16 }</code></pre>
<p>Here, the named pizza sauce must be a lowercase string of no more than 16 characters.  Note here also that constraint arrays can be nested.</p>
<h3 id="test-method-parameters">Test Method Parameters</h3>
<p>Other target object properties can be used as parameters to test methods in a constraint.</p>
<pre><code class="language-yaml">constrain:
  email:
    - email
  emailConfirmation:
    - { test: equal, params: $_.email }</code></pre>
<p>The above ensures that 'emailConfirmation' is equal to 'email'.</p>
<p>In <code>param</code> and <code>params</code>, you can reference a property of the target object by prefixing it with a dollar-sign (<code>$</code>).</p>
<p>Additionally, you can specify:</p>
<ul>
<li>
<p><code>_</code> - current target object<br />
The current target being validated (same as <code>s</code> unless nested)</p>
</li>
<li><code>__</code> - parent target object<br />
The parent of the object being validated. Use these with the dot-notation to check higher-level parents.</li>
</ul>
<h3 id="aggregate-constraints">Aggregate Constraints</h3>
<p>A neat feature of <em>straints</em> is the ability to validate against aggregate validation results.</p>
<p>The way this works is through the <code>poll</code> constraint object parameter.</p>
<p>Consider the following VSD.</p>
<pre><code class="language-yaml">activity:
  constrain:
    participants: [ not.missing, array ]
  nested:
    participants:
      constrain:
        _:
          - { name: enoughAdults, poll: age:is.atLeast21, results: passCount:atLeast2 }
      nested:
        ____:
          constrain:
            name: [ exists, string ]
            age: [ exists, number ]
is:
  - { name: atLeast21, test: not.less, params: [ 21 ] }
  - { name: atLeast2, test: not.less, params: [ 2 ] }</code></pre>
<p>There is quite a bit going on here, so lets break it down.</p>
<ol>
<li>
<p>The context 'activity' stipulates that an object must have an array of 'participants', each having a 'name' and 'age' that are a string and a number, respectively.</p>
</li>
<li>
<p>The 'enoughAdults' <code>poll</code> constraint stipulates that 'age' must be at least 21 (See <a href="/docs/vsd/rules.html">rules</a> page for more info on this syntax) for every object value of the current target.  </p>
<blockquote>
<p><strong>NOTE</strong><br />
Remember that the target value of <code>poll</code> is <strong>always</strong> the current target object, and it aims to aggregate validation results across all property values of that object.  The property to which a <code>poll</code> constraint is attached is merely the one to be blamed for any error that may result.  To emphasize this point (or cause further confusion), the special <code>_</code> property being used here actually refers to the current target object, which works as if the object had a property that referred to itself.</p>
</blockquote>
</li>
<li>The <code>results</code> rule in the <code>poll</code> constraint is evaluated against a separate aggregate results object (described below), and here we are checking that 'passCount', the number of values that passed the <code>poll</code> validation, is at least two.</li>
</ol>
<p>So, in a nutshell, this validation scheme is ensuring that a given activity has some participants (with names and ages) and that at least two of them are &quot;adults&quot; 21 or older.  We don't want to see any children getting hurt, of course.</p>
<h3 id="the-aggregate-results-object">The Aggregate Results Object</h3>
<p>Here are the properties of a <code>poll</code> constraint's aggregate object that is made available in <code>results</code>.</p>
<ul>
<li>
<p><strong>valid</strong> (boolean)<br />
This is <code>true</code> if all tests passed.  Also, if a <code>results</code> test is not provided on a <code>poll</code> constraint then this is the value that will be returned from the constraint.</p>
</li>
<li>
<p><strong>passed</strong> (array)<br />
Names of properties that passed</p>
</li>
<li>
<p><strong>failed</strong> (array)<br />
Names of properties that failed</p>
</li>
<li>
<p><strong>tested</strong> (array)<br />
Names of properties that were tested</p>
</li>
<li>
<p><strong>passCount</strong> (number)<br />
Number of properties that passed</p>
</li>
<li>
<p><strong>failCount</strong> (number)<br />
Number of properties that failed</p>
</li>
<li><strong>testCount</strong> (number)<br />
Number of properties that were tested</li>
</ul>
          </section>

        </div>
      </main>

      <footer>
        <div class="container-fluid">
          <p class="text-muted">
            <span>
              <a href="https://bitbucket.org/captison/straints.git" title="repository">repo</a>
            </span>
            <span>
              <a href="https://bitbucket.org/captison/straints/src/master/CHANGELOG.md" title="changelog">changelog</a>
            </span>
            <span>
              <a href="https://bitbucket.org/captison/straints/issues" title="feedback">feedback</a>
            </span>
            <span>
              <a href="https://www.npmjs.com/package/alyze" title="validator">alyze</a>
            </span>
            <span>
              <a href="http://couscous.io" title="markdown website generator">couscous</a>
            </span>
            <span>
              &copy; 2017 <a href="http://captison.com" title="straints author">Captison</a>
            </span>
          </p>
        </div>
      </footer>
    </div>
        <!-- Livereload -->
        <script>
            document.write('<script src="http://' + (location.host || 'localhost').split(':')[0] +
                    ':35729/livereload.js?snipver=1"></' + 'script>')
        </script>
        <!-- End Livereload -->
    </body>
</html>
