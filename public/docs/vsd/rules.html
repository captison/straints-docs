<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Straints | Documentation</title>

    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/font-awesome.min.css">
    <link rel="stylesheet" href="/css/highlight.dark.css">
    <link rel="stylesheet" href="/css/common.css">
    <link rel="stylesheet" href="/css/main.css">

    <script src="//code.jquery.com/jquery-1.12.4.min.js"></script>
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
    <script src="//yastatic.net/highlightjs/8.2/highlight.min.js"></script>

    <script>
      $(function() { hljs.initHighlightingOnLoad(); });
    </script>
  </head>
  <body>
    <div class="inner">

      <main class="container-fluid">
        <div class="row">

          <nav id="sidebar" class="col-sm-3 col-lg-2" role="navigation">
            <div class="logo">
              <a href="/index.html">
                <img src="/images/straints-logo.png" />
              </a>
            </div>
            <ul class="nav nav-pills nav-stacked">
                              <li class="starting ">
                  <a href="/docs/index.html">Getting Started</a>
                                  </li>
                              <li class="vsd ">
                  <a href="/docs/vsd/intro.html">The VSD</a>
                                      <ul>
                                              <li class="vsd-contexts ">
                          <a href="/docs/vsd/contexts.html">Contexts</a>
                        </li>
                                              <li class="vsd-constraints ">
                          <a href="/docs/vsd/constraints.html">Constraints</a>
                        </li>
                                              <li class="vsd-rules active">
                          <a href="/docs/vsd/rules.html">Rules</a>
                        </li>
                                          </ul>
                                  </li>
                              <li class="running ">
                  <a href="/docs/running.html">Running Validations</a>
                                  </li>
                              <li class="glossary ">
                  <a href="/glossary.html">Glossary</a>
                                  </li>
                              <li class="playground ">
                  <a href="/playground.html">Straints Playground</a>
                                  </li>
                          </ul>
          </nav>

          <section id="content" class="col-sm-offset-3 col-lg-offset-2 col-sm-9 col-lg-10">
            <h1 id="constraint-rules">Constraint Rules</h1>
<p>A rule is a reference to a validation unit (test method, context, constraint, constraint array) in the VSD, optionally prefixed by a target property.</p>
<p>They can be used</p>
<ul>
<li>in a constraint <code>test</code>, <code>poll</code>, <code>results</code>, or <code>if</code> parameters.</li>
<li>in an <code>include</code> condition <code>if</code> parameter.</li>
</ul>
<h2 id="reference-marks">Reference Marks</h2>
<p>Generally <em>straints</em> can figure out which validation unit a rule refers to.  However, it is possible that the name of a test method could clash with the name of a context or constraint reference.  In these cases, you can use an atmark (<code>@</code>) prefix for a context reference or a hashmark (<code>#</code>) prefix for a validator method to force <em>straints</em> to evaluate accordingly.</p>
<p>For example,</p>
<pre><code class="language-yaml">constrain:
  name: [ exists, string ]</code></pre>
<p>is the same as</p>
<pre><code class="language-yaml">constrain:
  name: [ '#exists', '#string' ]</code></pre>
<p>and (assuming 'address' is an object on the validation target and 'validAddress' is a context defined elsewhere)</p>
<pre><code class="language-yaml">constrain:
  address: [ validAddress ]</code></pre>
<p>is the same as</p>
<pre><code class="language-yaml">constrain:
  address: [ '@validAddress' ]</code></pre>
<h2 id="setting-the-target-property">Setting the Target Property</h2>
<p>Prefix a rule reference with a property name using a colon (<code>:</code>) to have it evaluate that property in the current object.</p>
<pre><code class="language-yaml">constrain:
  nameOne: [ exists, alphanumeric ]
  nameTwo: [ nameOne:#not.numeric ]</code></pre>
<p>Here, 'nameTwo' is only valid if 'nameOne' is not numeric.</p>
<h2 id="rule-as-constrain-key">Rule as Constrain Key</h2>
<p>A rule can also be used as a <code>constrain</code> key to be applied to properties when prefixed with a tilde (<code>~</code>).</p>
<pre><code class="language-yaml">constrain:
  ~exists: [ nameOne ]
  ~alphanumeric: [ nameOne ]
  ~nameOne:#not.numeric: [ nameTwo ]</code></pre>
<h3 id="inline-test-method-parameters">Inline Test Method Parameters</h3>
<p>A test method rule reference may also use have simple parameters applied inline.</p>
<p>Recall that there are two ways to pass test method parameters in a constraint.</p>
<ul>
<li>
<p><code>param</code> (<code>!</code>)<br />
Passes an array of values as a single parameter to the test method.</p>
</li>
<li><code>params</code> (<code>?</code>)<br />
Passes an array of values as individual parameters to the test method.</li>
</ul>
<p>Inline parameters are passed directly to a test method reference by appending a <code>!</code> or <code>?</code> followed by a colon-delimited list.</p>
<pre><code class="language-yaml">constrain:
  rgbComponent: [ itemIn!red:green:blue ]
  colorValue: [ between?0:255 ]</code></pre>
<p>When using inline parameters, remember that</p>
<ul>
<li>each one is parsed as a literal with a fallback to string</li>
<li>they cannot contain whitespace as this will cause rule reference parsing to fail</li>
<li>they will override parameters applied to a constraint via <code>param</code> or <code>params</code></li>
</ul>
<h1 id="constraint-rule-expressions">Constraint Rule Expressions</h1>
<p>A rule expression is two or more rules compared by logic gates, or even a single rule prefixed by a 'not' switch.</p>
<pre><code class="language-yaml">constrain:
  confirm: [ missing or true ]
  details: [ not exists or (confirm:true and @detail) ]</code></pre>
<p>This example says that 'confirm' is either missing or <code>true</code>. Property 'details' must either not exist or, if 'confirm' is <code>true</code>, must validate against the context specification 'detail'.</p>
<p>Rule expressions are simply evaluated from left to right so use parentheses for grouping as necessary.</p>
<p>For <code>x</code> in <code>A x B</code> you can use the following logic gates:</p>
<ul>
<li><strong>and</strong> - Both A and B must be true.</li>
<li><strong>or</strong> - At least one of A or B must be true.</li>
<li><strong>nor</strong> - Both A and B must be false.</li>
<li><strong>nand</strong> - At least one of A or B must be false.</li>
<li><strong>xnor</strong> - A and B must be the same boolean value.</li>
<li><strong>xor</strong> - A and B must not be the same boolean value.</li>
</ul>
<p>You may also use <code>not</code> in a rule expression to negate the test after it.</p>
<h1 id="rules-become-constraints">Rules Become Constraints</h1>
<p>Behind the scenes, <em>straints</em> ultimately works with constraint objects as it applies validations to object properties.  Therefore, standalone rules and rule expressions found in the VSD need to be converted into constraint objects.</p>
<p><em>Straints</em> uses a 'path' attribute on the constraint to identify it.  The value of 'path' is how you would reference the constraint from a results object.</p>
<pre><code class="language-js">instance.validate( ... ).then(results =&gt;
{
    var constraint = results.constraints[CONSTRAINT_NAME];
    ...
});</code></pre>
<h4 id="test-methods">Test Methods</h4>
<p>When a test method (without inline parameters) is specified as a constraint</p>
<pre><code class="language-yaml">my_context:
  constrain:
    name: [ method ]</code></pre>
<p>the resulting constraint becomes</p>
<pre><code class="language-js">{ test: '#method', path: '#method' }</code></pre>
<p>When a test method (with inline parameters) is specified as a constraint</p>
<pre><code class="language-yaml">my_context:
  constrain:
    name: [ method!7:apple ]</code></pre>
<p>the resulting constraint becomes</p>
<pre><code class="language-js">{ test: 'method!7:apple', path: 'my_context.constrain.name.0' }</code></pre>
<h4 id="contexts">Contexts</h4>
<p>When a context is specified as a constraint</p>
<pre><code class="language-yaml">my_context:
  constrain:
    address: [ context ]</code></pre>
<p>the resulting constraint becomes</p>
<pre><code class="language-js">{ test: '@context', path: '@context' }</code></pre>
<h4 id="property-prefixed">Property Prefixed</h4>
<p>When a property prefixed rule is specified</p>
<pre><code class="language-yaml">my_context:
  constrain:
    address: [ property:method ]</code></pre>
<p>the resulting constraint becomes</p>
<pre><code class="language-js">{ test: 'property:method', path: 'property:method' }</code></pre>
<h4 id="rule-expressions">Rule Expressions</h4>
<p>When a rule expression is specified as a constraint</p>
<pre><code class="language-yaml">my_context:
  constrain:
    address: [ not @context or property:#method ]</code></pre>
<p>the resulting constraint becomes</p>
<pre><code class="language-js">{ test: 'not @context or property:#method', path: 'my_context.constrain.address.0' }</code></pre>
<h1 id="some-caveats">Some Caveats</h1>
<p>You should avoid doing the following:</p>
<ol>
<li>
<p><strong>Don't reference an array of constraints in a rule expression</strong><br />
This is not supported yet, but may be in the future.</p>
</li>
<li>
<p><strong>Don't reference a constraint with an <code>if</code> condition in a rule expression</strong><br />
If the <code>if</code> fails, the constraint will return <code>null</code> and destroy the expression.</p>
</li>
<li><strong>Don't use property names that contain anything other than ASCII-based javascript identifier characters</strong><br />
This limitation may be resolved in the future.</li>
</ol>
          </section>

        </div>
      </main>

      <footer>
        <div class="container-fluid">
          <p class="text-muted">
            <span>
              <a href="https://bitbucket.org/captison/straints.git" title="repository">repo</a>
            </span>
            <span>
              <a href="https://bitbucket.org/captison/straints/src/master/CHANGELOG.md" title="changelog">changelog</a>
            </span>
            <span>
              <a href="https://bitbucket.org/captison/straints/issues" title="feedback">feedback</a>
            </span>
            <span>
              <a href="https://www.npmjs.com/package/alyze" title="validator">alyze</a>
            </span>
            <span>
              <a href="http://couscous.io" title="markdown website generator">couscous</a>
            </span>
            <span>
              &copy; 2017 <a href="http://captison.com" title="straints author">Captison</a>
            </span>
          </p>
        </div>
      </footer>
    </div>
        <!-- Livereload -->
        <script>
            document.write('<script src="http://' + (location.host || 'localhost').split(':')[0] +
                    ':35729/livereload.js?snipver=1"></' + 'script>')
        </script>
        <!-- End Livereload -->
    </body>
</html>
