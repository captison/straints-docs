<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Straints | Documentation</title>

    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/font-awesome.min.css">
    <link rel="stylesheet" href="/css/highlight.dark.css">
    <link rel="stylesheet" href="/css/common.css">
    <link rel="stylesheet" href="/css/main.css">

    <script src="//code.jquery.com/jquery-1.12.4.min.js"></script>
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
    <script src="//yastatic.net/highlightjs/8.2/highlight.min.js"></script>

    <script>
      $(function() { hljs.initHighlightingOnLoad(); });
    </script>
  </head>
  <body>
    <div class="inner">

      <main class="container-fluid">
        <div class="row">

          <nav id="sidebar" class="col-sm-3 col-lg-2" role="navigation">
            <div class="logo">
              <a href="/index.html">
                <img src="/images/straints-logo.png" />
              </a>
            </div>
            <ul class="nav nav-pills nav-stacked">
                              <li class="starting ">
                  <a href="/docs/index.html">Getting Started</a>
                                  </li>
                              <li class="vsd ">
                  <a href="/docs/vsd/intro.html">The VSD</a>
                                      <ul>
                                              <li class="vsd-contexts ">
                          <a href="/docs/vsd/contexts.html">Contexts</a>
                        </li>
                                              <li class="vsd-constraints ">
                          <a href="/docs/vsd/constraints.html">Constraints</a>
                        </li>
                                              <li class="vsd-rules ">
                          <a href="/docs/vsd/rules.html">Rules</a>
                        </li>
                                          </ul>
                                  </li>
                              <li class="running active">
                  <a href="/docs/running.html">Running Validations</a>
                                  </li>
                              <li class="glossary ">
                  <a href="/glossary.html">Glossary</a>
                                  </li>
                              <li class="playground ">
                  <a href="/playground.html">Straints Playground</a>
                                  </li>
                          </ul>
          </nav>

          <section id="content" class="col-sm-offset-3 col-lg-offset-2 col-sm-9 col-lg-10">
            <h1 id="running-validations">Running Validations</h1>
<p>To run a validation call <code>.validate()</code> on a <em>straints</em> instance.</p>
<p>For example, given the following arguments,</p>
<pre><code class="language-js">var target = { username: 'alexa123', password: 'partytime56' };
var contexts = [ 'login' ];
var validate = (result, info) =&gt; { ... };
var success = results =&gt; { ... };
var failure = results =&gt; { ... };</code></pre>
<p>validation can be run like so</p>
<pre><code class="language-js">instance.validate(target, contexts, validate).then(success, failure); // promise</code></pre>
<p>Here is a quick explanation of the arguments.</p>
<ul>
<li>
<p><strong>target</strong> (object)<br />
The object to be validated.</p>
</li>
<li>
<p><strong>contexts</strong> (string|array)<br />
One or more contexts to be validated against.  This can be a comma-delimited string or an array.</p>
</li>
<li>
<p><strong>validate(result: boolean, info: object)</strong><br />
Callback executed for every constraint test during validation.</p>
</li>
<li>
<p><strong>success(results: object)</strong><br />
Callback to be executed when validation completes successfully.</p>
</li>
<li><strong>failure(results: object)</strong><br />
Callback to be executed if an error occurred during validation.</li>
</ul>
<h2 id="overriding-a-validation">Overriding A Validation</h2>
<p>Individual validations can be managed by providing a function as the last parameter to <code>.validate()</code>.</p>
<p>This callback will receive the following parameters</p>
<ul>
<li>
<p><strong>result</strong> (boolean)<br />
The boolean result of the validation test.  </p>
</li>
<li>
<p><strong>info</strong> (object)<br />
Additional information pertaining to the validation test.  </p>
<ul>
<li>
<p><strong>target</strong> (object)<br />
Current validation target object (same as <code>starget</code> unless nested)  </p>
</li>
<li>
<p><strong>starget</strong> (object)<br />
Session validation target object (always the original validation target)</p>
</li>
<li>
<p><strong>name</strong> (string)<br />
Current target object property name</p>
</li>
<li>
<p><strong>sname</strong> (string)<br />
Session target object property name</p>
</li>
<li>
<p><strong>rule</strong> (object)<br />
Validating constraint object</p>
</li>
<li><strong>level</strong> (string)<br />
Name of the current validation level.</li>
</ul>
</li>
</ul>
<p>If this callback is provided then it MUST return a boolean value.  Simply return <code>result</code> if nothing else.</p>
<pre><code class="language-js">function(result, info) { return result; }</code></pre>
<h2 id="validation-results">Validation Results</h2>
<p>The validation results are provided to both the <code>success</code> and <code>failure</code> callbacks.  In case of failure, obviously, the results object may not be complete.</p>
<p>You will find the following items in this object.</p>
<ul>
<li>
<p><strong>target</strong> (object)<br />
The object originally passed to <code>.validate()</code>.</p>
</li>
<li>
<p><strong>contexts</strong> (array)<br />
The contexts involved in the validation session.  This includes the contexts passed to <code>.validate()</code> as well as any included by those contexts, recursively.</p>
</li>
<li>
<p><strong>tested.xxx</strong> (objects)<br />
Map of property to constraint ids of constraints whose tests were specified at validation level 'xxx'.</p>
</li>
<li>
<p><strong>constraints</strong> (object)<br />
All of the constraints directly involved in the validation session keyed by their ids.</p>
</li>
<li>
<p><strong>isComplete</strong> (boolean)<br />
A boolean value that will be true if validation completed successfully.</p>
</li>
<li>
<p><strong>error</strong> (any)<br />
If the <code>failure</code> callback is executed, this will have error information.</p>
</li>
<li>
<p><strong>findConstraints(pname: string, level: string, value: boolean|null): array</strong><br />
<strong>findProperties(cname: string, level: string, value: boolean|null): array</strong><br />
Functions that return an array of constraint or property names by property or constraint, validation level, and value.</p>
<ul>
<li>
<p><strong>pname</strong> (string)<br />
Object property name to get constraints for. If not given then all properties are checked.</p>
</li>
<li>
<p><strong>cname</strong> (string)<br />
Constraint identifier to get properties for. If not given then all constraints are checked.</p>
</li>
<li>
<p><strong>level</strong> (string)<br />
The validation level to check. If not given then <code>constrain</code> is assumed.</p>
</li>
<li><strong>value</strong> (boolean|null)<br />
Value to check for. This can only be <code>true</code>, <code>false</code>, or <code>null</code>. If not given then <code>false</code> is assumed.  </li>
</ul>
</li>
<li>
<p><strong>validFor(level: string): boolean</strong><br />
For the given validation level, returns <code>true</code> if all tests passed, <code>false</code> if any failed, and <code>null</code> if none were run or if the level does not exist.</p>
</li>
<li>
<p><strong>valid(): boolean</strong><br />
Returns true if validation completed successfully and no <code>constrain</code>-level tests failed.</p>
</li>
<li><strong>payload(cname: string): any</strong><br />
Returns the payload data for the named constraint.</li>
</ul>
<h3 id="for-example">For Example...</h3>
<p>For a given target object and <em>straints</em> instance:</p>
<pre><code class="language-js">var target = { name: 'Alexa', birthdate: null, attractive: true };
var contexts = [ 'contextOne', 'contextThree' ];
var instance = straints({ levels: [ 'suggest' ] });</code></pre>
<p>Here's a simple example that illustrates what you might do when validation fails.</p>
<pre><code class="language-js">instance.validate(target, contexts).then
(
    results =&gt;
    {
        if (!results.valid())
        {
            // names of constraints that failed for property 'name'...
            var failNames = results.findConstraints('name');
            // then get the constraint objects that failed
            var constraints = failNames.map(name =&gt; results.constraints[name]);
            // or get names of all failing properties
            var failProps = results.findProperties();
        }    
    }
);</code></pre>
<p>Similarly, if you want to check those validation suggestions...</p>
<pre><code class="language-js">instance.validate(target, contexts).then
(
    results =&gt;
    {
        if (!results.validFor('suggest'))
        {
            // names of constraints that failed for property 'name'...
            var failNames = results.findConstraints('name', 'suggest');
            // then get the constraint objects that failed
            var constraints = failNames.map(name =&gt; results.constraints[name]);
            // or get names of all failing properties
            var failProps = results.findProperties(null, 'suggest');
        }
    }
);</code></pre>
<blockquote>
<p><strong>NOTE:</strong><br />
The results object collects information about the validation session as it runs, thus its state is undetermined if validation fails to complete.  However, it is passed to the <code>failure</code> callback nonetheless.  In this scenario, only its <code>target</code>, <code>isComplete</code>, and <code>error</code> properties can be relied upon.</p>
</blockquote>
          </section>

        </div>
      </main>

      <footer>
        <div class="container-fluid">
          <p class="text-muted">
            <span>
              <a href="https://bitbucket.org/captison/straints.git" title="repository">repo</a>
            </span>
            <span>
              <a href="https://bitbucket.org/captison/straints/src/master/CHANGELOG.md" title="changelog">changelog</a>
            </span>
            <span>
              <a href="https://bitbucket.org/captison/straints/issues" title="feedback">feedback</a>
            </span>
            <span>
              <a href="https://www.npmjs.com/package/alyze" title="validator">alyze</a>
            </span>
            <span>
              <a href="http://couscous.io" title="markdown website generator">couscous</a>
            </span>
            <span>
              &copy; 2017 <a href="http://captison.com" title="straints author">Captison</a>
            </span>
          </p>
        </div>
      </footer>
    </div>
        <!-- Livereload -->
        <script>
            document.write('<script src="http://' + (location.host || 'localhost').split(':')[0] +
                    ':35729/livereload.js?snipver=1"></' + 'script>')
        </script>
        <!-- End Livereload -->
    </body>
</html>
