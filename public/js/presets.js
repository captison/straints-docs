window.straints_presets =
{
    "Automobile":
    {
        validator:
        {
            onMissingValue: false,
            emptyStringIsMissing: true
        },

        levels: null,

        schema:
        {
            vehicle:
            {
                constrain:
                {
                    make: [ 'not.missing', 'auto.maker' ],
                    model: [ 'not.missing', 'string', 'uppercase!-1' ],
                    year: [ 'number', 'less!2020', 'not.less!2000']
                }
            },

            auto:
            [
                { name: 'maker', test: 'itemIn', param: [ 'Ford', 'Nissan', 'Hyundai', 'Mercedes Benz' ] }
            ]
        },

        contexts: "vehicle",

        target:
        {
            make: 'Chevrolet',
            model: 'corvette',
            year: 2004
        }
    },

    "Egg Nog Recipe":
    {
        validator:
        {
            onMissingValue: true,
            emptyStringIsMissing: true
        },

        levels: 'advise',

        schema:
        {
            required:
            {
                constrain:
                {
                    '~not.missing': [ 'eggs', 'milk', 'sugar', 'cream', 'steps' ]
                }
            },
            recipe:
            {
                include: 'required',
                constrain:
                {
                    '~string': [ 'milk', 'sugar', 'cream' ],
                    eggs: [ 'number', 'not.more!6' ],
                    milk: [ 'measure.cup' ],
                    sugar: [ 'measure.cup' ],
                    cream: [ 'measure.cup' ],
                    steps: [ 'array' ]
                },
                advise:
                {
                    vanilla: [ 'string', 'measure.tbsp' ],
                    nutmeg: [ 'string', 'measure.tbsp or measure.teas' ]
                },
                nested:
                {
                    steps:
                    {
                        constrain:
                        {
                            ____: [ 'string' ]
                        }
                    }
                }
            },
            measure:
            [
                { name: 'cup', test: 'match', params: /^[0-9]+\s?cups$/ },
                { name: 'tbsp', test: 'match', params: /^[0-9]+\s?tbsp[.]?$/ },
                { name: 'teas', test: 'match', params: /^[0-9]+\s?teas[.]?$/ }
            ]
        },

        contexts: "recipe",

        target:
        {
            cream: '3 cups',
            vanilla: '1 tbsp.',
            milk: '3 cups',
            sugar: '2 cup',
            nutmeg: '1 teas.',
            steps:
            [
                'Mix everything in a large bowl.',
                'Keep mixing!',
                'Mix some more.',
                'Chill the mixture for 3 days',
                'Enjoy!'
            ]
        }
    },

    "Player Details":
    {
        validator:
        {
            onMissingValue: true,
            emptyStringIsMissing: true
        },

        levels: null,

        schema:
        {
            player:
            {
                include: [ 'names', 'numbers' ]
            },
            names:
            {
                constrain:
                {
                    firstname: [ 'not.missing', 'string' ],
                    lastname: [ 'not.missing', 'string' ],
                    nickname: [ 'alphanumeric', 'not.longer!16' ]
                }
            },
            numbers:
            {
                constrain:
                {
                    height: [ 'not.missing', 'is.height' ],
                    weight: [ 'is.weight' ],
                    age: [ 'not.missing', 'number' ]
                }
            },
            is:
            [
                { name: 'height', test: 'match', params: /^[0-9]+(cm|in)$/ },
                { name: 'weight', test: 'match', params: /^[0-9]+(kg|lbs)$/ }
            ]
        },

        contexts: "names",

        target:
        {
            firstname: 'Sparta',
            lastname: 'Cuss',
            height: '72in',
            weight: 200,
            nickname: 'for.the.money.win',
            age: 1473
        }
    }
}
