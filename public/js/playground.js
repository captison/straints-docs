
$(document).ready(function()
{
    var Playground = function() { return this.init(); }

    Playground.prototype =
    {
        init: function()
        {
            this.html =
            {
                body: function(i, c)
                    { return '<div id="' + i + '" class="collapse">' + c + '</div>' },
                head: function(t, c)
                    { return '<div class="head" data-toggle="collapse" data-target="#' + t + '">' + c + '</div>' },
                code: function(c)
                    { return '<code>' + c + '</code>' },
                section: function(c)
                    { return '<section>' + c + '</section>' },
                table: function(c)
                    { return '<table class="table table-condensed table-striped">' + c + '</table>' },
                tr: function(c)
                    { return '<tr>' + c + '</tr>' },
                td: function(c)
                    { return '<td>' + c + '</td>' }
            }

            this.fields =
            {
                onMissingValue: $('#alyze_onMissingValue'),
                emptyStringIsMissing: $('#alyze_emptyStringIsMissing'),
                contexts: $('#straints_contexts'),
                levels: $('#straints_levels'),
                schema: $('#straints_schema'),
                target: $('#straints_target')
            }

            this.buttons =
            {
                clear: $('#button_clear').on('click', this.onClear.bind(this)),
                collapse: $('#button_collapse').on('click', this.onCollapse.bind(this)),
                expand: $('#button_expand').on('click', this.onExpand.bind(this)),
                validate: $('#button_validate').on('click', this.onValidate.bind(this))
            }

            this.resultsDisplay = $('#results');
            this.panels = $('.panel .panel-body').collapse('hide');
            this.modal = $('#straints_presets');

            $('#preset_list').html(this.htmlPresetList(window.straints_presets))
                .select('li').on('click', this.onPresetClick.bind(this));

            this.logs = [];
            var logger = function() { this.logs.push(Array.prototype.slice.call(arguments).join(' ')); }.bind(this);
            alyze.config({ log: logger });

            this.replacer = function(k,v) { return v instanceof RegExp ? v.source : v };
        },

        attempt: function(action)
        {
            try { action(); return true; } catch(e) { return false; }
        },

        clear: function()
        {
            this.fields.onMissingValue.prop('checked', false);
            this.fields.emptyStringIsMissing.prop('checked', false);
            this.fields.contexts.val('');
            this.fields.levels.val('');
            this.fields.schema.val('');
            this.fields.target.val('');
            this.resultsDisplay.html('No results to display.');
        },

        failResults: function(results)
        {
            this.resultsDisplay.html(this.htmlError(results.error));
        },

        getContexts: function()
        {
            var contexts = this.fields.contexts.val();

            if (!contexts.trim())
                throw new Error('contextMissing');

            return contexts;
        },

        getLevels: function()
        {
            var levels = this.fields.levels.val().trim();

            return levels ? levels.split(/\s*,\s*/) : [];
        },

        getTarget: function()
        {
            var target = this.fields.target.val().trim();

            if (!target)
                throw new Error('targetMissing');

            return this.toJson(target, 'targetParse');
        },

        getSchema: function()
        {
            var schema = this.fields.schema.val().trim();

            if (!schema)
                throw new Error('schemaMissing');

            return this.toJson(schema, 'schemaParse');
        },

        htmlError: function(error)
        {
            var wrap = function(m) { return '<span class="error">' + m + '</span>'; };

            switch (error.message)
            {
                case 'contextMissing':
                    return wrap('Please provide at least one validation context');
                case 'schemaMissing':
                    return wrap('You must provide a validation schema');
                case 'schemaParse':
                    return wrap('Validation Schema Definition could not be parsed as JSON or YAML');
                case 'targetMissing':
                    return wrap('You must provide a validation target');
                case 'targetParse':
                    return wrap('The target could not be parsed as JSON or YAML');
                default:
                    return wrap(error);
            }
        },

        htmlLogs: function()
        {
            var html = this.html;
            var id = 'log_messages';
            var logHtml = 'No Messages'

            if (this.logs.length > 0)
                logHtml = this.logs.map(function(l) { return '<div>' + l + '</div>'; }).join('');

            return html.head(id, 'Log Messages') + html.body(id, logHtml);
        },

        htmlFailedProps: function(results, level)
        {
            var html = this.html;
            var id = 'failed_' + level;
            var out = null;
            var properties = results.findProperties(null, level);

            if (properties.length > 0)
            {
                var body = '';

                properties.forEach(function(p)
                {
                    body = body + html.tr(html.td(p) + html.td(results.findConstraints(p, level).join(', ')));
                });

                out = html.table(body);
            }
            else
            {
                out = 'No tests failed!';
            }

            return html.head(id, 'Failed Property Tests [' + level + ']') + html.body(id, out);
        },

        htmlPresetList: function(presets)
        {
            return Object.keys(presets).map(function(k)
            {
                return '<li class="list-group-item" data-value="' + k + '">' + k + '</li>';
            });
        },

        htmlResultsObject: function(results)
        {
            var html = this.html;
            var id = 'fullresults';

            return html.head(id, 'Full Results Object') + html.body(id, html.code(JSON.stringify(results, null, 4)));
        },

        makeConfiguration: function()
        {
            var config =
            {
                schema: this.getSchema(),
                validator: this.makeValidator(),
                levels: this.getLevels()
            }

            return config;
        },

        makeValidator: function()
        {
            var config = { onMissingValue: this.fields.onMissingValue.prop('checked') };

            if (this.fields.emptyStringIsMissing.prop('checked'))
                config.asMissing = [ '' ];

            return alyze.create(config);
        },

        onCollapse: function()
        {
            this.panels.collapse('hide');
        },

        onClear: function(e)
        {
            this.clear();
        },

        onExpand: function()
        {
            this.panels.collapse('show');
        },

        onPresetClick: function(e)
        {
            // var presetName = $("input[type='radio'][name='presets']:checked").val();
            var preset = window.straints_presets[$(e.target).attr('data-value')];

            this.clear();

            if (preset.validator.onMissingValue) this.fields.onMissingValue.prop('checked', true);
            if (preset.validator.emptyStringIsMissing) this.fields.emptyStringIsMissing.prop('checked', true);
            this.fields.contexts.val(preset.contexts);
            this.fields.levels.val(preset.levels);
            this.fields.schema.val(JSON.stringify(preset.schema, this.replacer, 4));
            this.fields.target.val(JSON.stringify(preset.target, this.replacer, 4));

            this.panels.collapse('show');
            this.modal.modal('hide');
        },

        onValidate: function()
        {
            $('#results.panel-body').collapse('show');
            $('html, body').animate({ scrollTop: $('#results_area').offset().top }, 'slow');


            try
            {
                this.logs = [];
                straints(this.makeConfiguration()).validate(this.getTarget(), this.getContexts()).then
                    (this.showResults.bind(this), this.failResults.bind(this));
            }
            catch (e)
            {
                this.resultsDisplay.html(this.htmlError(e));
            }
        },

        showResults: function(results)
        {
            var html = this.html;
            var levels = ['constrain'].concat(this.getLevels());

            var mapLevels = function(l) { return html.section(this.htmlFailedProps(results, l)); }.bind(this);

            var htmlFailedProps = levels.map(mapLevels).join('');
            var htmlResultsObject = html.section(this.htmlResultsObject(results));
            var htmlLogs = html.section(this.htmlLogs());

            this.resultsDisplay.html(htmlFailedProps + htmlResultsObject + htmlLogs);
        },

        toJson: function(stringData, errorKey)
        {
            var parsed = null;

            var tryJson = function() { parsed = JSON.parse(stringData); };
            var tryYaml = function() { parsed = jsyaml.safeLoad(stringData); };

            if (!(this.attempt(tryJson) || this.attempt(tryYaml)))
                throw new Error(errorKey);

            return parsed;
        }
    }

    new Playground();
});
